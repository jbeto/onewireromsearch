typedef unsigned char uint8_t;
typedef unsigned long long uint64_t;
typedef int GPIO_TypeDef;

typedef struct tagTIBOARDPIN {
    GPIO_TypeDef* port;
    int pin;
} TIBOARDPIN;

typedef struct tagADDRESSLIST {
    uint64_t address;
    int connected;
    struct tagADDRESSLIST * next;
} ADDRESSLIST;

void OneWireROMSeach(int numberOfPins, TIBOARDPIN tiBoardPins[], ADDRESSLIST **addressList);
