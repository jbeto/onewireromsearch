/*
 ============================================================================
 Name        : simu.h
 Author      : Heitmann
 Version     : 1.0
 Copyright   : Copyright 2009
 Description : 1-Wire Bus Simulation for ROM Search Algorithm
 ============================================================================
 */



#ifndef SIMU_H_
#define SIMU_H_

#define RESET       simu_reset()
#define RDBIT()     simu_rdbit()
#define WRBIT(b)    simu_wrbit(b)

int simu_getSensorCnt(void);
unsigned char* simu_getSensorCode(int i);
	
void simu_reset(void);
int  simu_rdbit(void);
void simu_wrbit( int b );


#endif /*SIMU_H_*/
