/*
 ============================================================================
 Name        : simu.h
 Author      : Heitmann
 Version     : 1.0
 Copyright   : Copyright 2009
 Description : 1-Wire Bus Simulation for ROM Search Algorithm
 ============================================================================
 */

/*
 * Diese Simulation bildet das Verhalten eines 1-Wire Busses waehrend 
 * der Sensorsuche nach. Hierzu stehen drei Funktionen zur Verfuegung: 
 * void simu_reset(), 
 * int simu_rdbit() und 
 * void simu_wrbit(int bit). 
 * Die Verwendung der Simulation beginnt mit dem Reset. 
 * Danach muss mit Hilfe von simu_wrbit das 
 * ROM-Search Kommando (0xF0) gesendet werden. 
 * Nur dieses Kommando ist zur Zeit implementiert. 
 * Nachdem das Kommando gesendet worden ist, 
 * muss der eigentliche Ablauf des Suchalgorithmusses gestartet werden,
 * der mit dem Lesen der ersten beiden Bits beginnt. 
 * Dazu simu_rdbit aufrufen. 
 */


#include <stdio.h>
#include "simu.h"

typedef struct {
	int state;
	unsigned char id[8];
} Sensor;

Sensor sensor[] = { 
		{ .id= {0x28,0x88,0x48,0xFF,0x00,0x00,0x00,0x69} }, 
		{ .id= {0x28,0x6C,0xF9,0xFE,0x00,0x00,0x00,0x91} },
		{ .id= {0x28,0xEA,0xDD,0xFE,0x00,0x00,0x00,0x2E} }, 
		{ .id= {0x28,0x9E,0xD0,0xFE,0x00,0x00,0x00,0x86} }, 
		{ .id= {0x28,0x9F,0x49,0xFF,0x00,0x00,0x00,0x7A} } };

/*
Sensor sensor[] = { 
 		{ .id= {0x28,0x12,0x34,0x01,0x00,0x00,0x00,0x00} }, 
 		{ .id= {0x28,0x12,0x34,0x02,0x00,0x00,0x00,0x00} }, 
 		{ .id= {0x28,0x12,0x34,0x03,0x00,0x00,0x00,0x00} }, 
 		{ .id= {0x28,0x12,0x34,0x04,0x00,0x00,0x00,0x00} }, 
 		{ .id= {0x28,0x12,0x34,0x05,0x00,0x00,0x00,0x00} }, 
 		{ .id= {0x28,0x12,0x34,0x06,0x00,0x00,0x00,0x00} } };
*/

#define SENSORCNT (sizeof(sensor)/sizeof(Sensor))

int simu_getSensorCnt(void) {
	return SENSORCNT;
}

unsigned char* simu_getSensorCode(int i) {
	return sensor[i].id;
}

void simu_reset(void) {
	int i;

	for (i = 0; i < SENSORCNT; i++) {
		sensor[i].state = 0;
	}
}

int simu_rdbit(void) {
	int i;
	int bit = 1;
	int ix;
	int m;

	for (i = 0; i < SENSORCNT; i++) {
		switch (sensor[i].state) {
	      case   8: case  11: case  14: case  17: case  20: case  23: case  26: case  29:
	      case  32: case  35: case  38: case  41: case  44: case  47: case  50: case  53:
	      case  56: case  59: case  62: case  65: case  68: case  71: case  74: case  77:
	      case  80: case  83: case  86: case  89: case  92: case  95: case  98: case 101:
	      case 104: case 107: case 110: case 113: case 116: case 119: case 122: case 125:
	      case 128: case 131: case 134: case 137: case 140: case 143: case 146: case 149:
	      case 152: case 155: case 158: case 161: case 164: case 167: case 170: case 173:
	      case 176: case 179: case 182: case 185: case 188: case 191: case 194: case 197:
			ix = (sensor[i].state-8 )/3;
			m = 1<<(ix%8);
			ix = ix>>3;

			bit = bit && (sensor[i].id[ix] & m);
			sensor[i].state++;
			break;
	      case   9: case  12: case  15: case  18: case  21: case  24: case  27: case  30:
	      case  33: case  36: case  39: case  42: case  45: case  48: case  51: case  54:
	      case  57: case  60: case  63: case  66: case  69: case  72: case  75: case  78:
	      case  81: case  84: case  87: case  90: case  93: case  96: case  99: case 102:
	      case 105: case 108: case 111: case 114: case 117: case 120: case 123: case 126:
	      case 129: case 132: case 135: case 138: case 141: case 144: case 147: case 150:
	      case 153: case 156: case 159: case 162: case 165: case 168: case 171: case 174:
	      case 177: case 180: case 183: case 186: case 189: case 192: case 195: case 198:
			ix = (sensor[i].state-8 )/3;
			m = 1<<(ix%8);
			ix = ix>>3;

			bit = bit && !(sensor[i].id[ix] & m);
			sensor[i].state++;
			break;

		default:
      sensor[i].state = 999;
			break;
		}
	}
	return bit;
}

void simu_wrbit(int b) {
	int i;
	int ix;
	int m;

	for (i = 0; i < SENSORCNT; i++) {
		switch (sensor[i].state) {
      case 0: if( b ){sensor[i].state=999;}else{sensor[i].state=  1;}break;
      case 1: if( b ){sensor[i].state=999;}else{sensor[i].state=  2;}break;
      case 2: if( b ){sensor[i].state=999;}else{sensor[i].state=  3;}break;
      case 3: if( b ){sensor[i].state=999;}else{sensor[i].state=  4;}break;
      case 4: if( b ){sensor[i].state=  5;}else{sensor[i].state=999;}break;
      case 5: if( b ){sensor[i].state=  6;}else{sensor[i].state=999;}break;
      case 6: if( b ){sensor[i].state=  7;}else{sensor[i].state=999;}break;
      case 7: if( b ){sensor[i].state=  8;}else{sensor[i].state=999;}break;

      case  10: case  13: case  16: case  19: case  22: case  25: case  28: case  31:
      case  34: case  37: case  40: case  43: case  46: case  49: case  52: case  55:
      case  58: case  61: case  64: case  67: case  70: case  73: case  76: case  79:
      case  82: case  85: case  88: case  91: case  94: case  97: case 100: case 103:
      case 106: case 109: case 112: case 115: case 118: case 121: case 124: case 127:
      case 130: case 133: case 136: case 139: case 142: case 145: case 148: case 151:
      case 154: case 157: case 160: case 163: case 166: case 169: case 172: case 175:
      case 178: case 181: case 184: case 187: case 190: case 193: case 196: case 199:

			ix = (sensor[i].state-8 )/3;
			m = 1<<(ix%8);
			ix = ix>>3;

			if ( (b ? 1 : 0 ) == ( (sensor[i].id[ix] & m) ? 1 : 0 )) {
				sensor[i].state++;
			} else {
				sensor[i].state = 999;
			}
			break;

		default:
      sensor[i].state = 999;
			break;
		}
	}
}

