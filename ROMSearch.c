/*
 ============================================================================
 Name        : ROMSearch.cpp
 Author      : Heitmann
 Version     : 1.0
 Copyright   : Copyright 2009
 Description : 1-Wire ROMSearch Algorithm
 ============================================================================
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "simu.h"
#include "test.h"

uint8_t* addressParser(uint64_t inputAddress, uint8_t returnAddress[8]) {
    int i;
    for (i = 0; i < 8; i++) {
        returnAddress[i] = 0;
    }

    uint64_t currentBit;

    for (i = 0; i < 64; i++) {
        currentBit = ((uint64_t) inputAddress & (1LL << i));

        if (currentBit)
            returnAddress[i / 8] |= 1LL << (i % 8);
    }

    return returnAddress;
}

const char *byte_to_binary(int x) {
    static char b[9];
    b[0] = '\0';

    int z;
    for (z = 128; z > 0; z >>= 1) {
        strcat(b, ((x & z) == z) ? "1" : "0");
    }

    return b;
}

void DisplayAddress(int line, uint8_t address[8]) {
    int j;

    printf("Sensor %d: ", line);
    for (j = 0; j < 8; j++) {
        printf("%02X ", address[j]);
    }
    printf("  ");
    for (j = 0; j < 8; j++) {
        printf("%s ", byte_to_binary(address[j]));
    }
    printf("\n");
}

int main() {
    int i;
    //int j;

    /* Alle ROMCodes der Sensoren ausgeben */
    /* Dies dient natuerlich nur zu Debug-Zwecken */
    for (i = 0; i < simu_getSensorCnt(); i++) {
        DisplayAddress(1, simu_getSensorCode(i));

        //        printf("Sensor %d: ", i);
        //        for (j = 0; j < 8; j++) {
        //            printf("%02X ", simu_getSensorCode(i)[j]);
        //        }
        //        printf("\n");
        //        printf("           ");
        //        for (j = 0; j < 8; j++) {
        //            printf("%s ", byte_to_binary(simu_getSensorCode(i)[j]));
        //        }
        //        printf("\n");
    }
    printf("\n");


    /* Hier koennte der Code fuer ROM-Search stehen */
    ADDRESSLIST* addressList = 0;
    uint8_t showThisAddress[8];
    i = 0;

    OneWireROMSeach(0, 0, &addressList);

    while (addressList != 0) {
        addressParser(addressList->address, showThisAddress);
        DisplayAddress(i, showThisAddress);

        //        printf("Sensor %d: ", i);
        //        for (j = 0; j < 8; j++) {
        //            printf("%02X ", showThisAddress[j]);
        //        }
        //        printf("\n");

        addressList = addressList->next;
        i++;
    }


    return 0;
}







