#include "test.h"

#include "simu.h"
#include <stdlib.h>

void OneWireSendBit(int numberOfPins, TIBOARDPIN tiBoardPins[], uint8_t bitToSend) {
    simu_wrbit(bitToSend);
}

uint8_t OneWireReceiveBit(int numberOfPins, TIBOARDPIN tiBoardPins[]) {
    return (uint8_t) simu_rdbit();
}

int OneWireSendReset(int numberOfPins, TIBOARDPIN tiBoardPins[]) {
    simu_reset();
    return 1;
}

void OneWireSendByte(int numberOfPins, TIBOARDPIN tiBoardPins[], uint8_t byteToSend) {
    int i;

    for (i = 0; i < 8; i++) {
        OneWireSendBit(numberOfPins, tiBoardPins, byteToSend & (1 << i));
    }
}

int OneWireIsDeviceConnected(int numberOfPins, TIBOARDPIN tiBoardPins[], uint64_t address) {
    return 1;
}

void AddressListSetAllDisconnected(ADDRESSLIST* list) {
    ADDRESSLIST* currentAddressListEntry = list;

    while (currentAddressListEntry) {
        currentAddressListEntry->connected = 0;
        currentAddressListEntry = currentAddressListEntry->next;
    }
}

void AddressListAddDevice(ADDRESSLIST** list, uint64_t addressFound) {
    ADDRESSLIST* currentAddressListEntry = *list;
    ADDRESSLIST* lastAddressListEntry = 0;
    //int i;

    while (currentAddressListEntry) {
        if (addressFound == currentAddressListEntry->address) {
            //Bereits in der Liste
            currentAddressListEntry->connected = 1;
            return;
        }

        lastAddressListEntry = currentAddressListEntry;
        currentAddressListEntry = currentAddressListEntry->next;
    }

    currentAddressListEntry = 0;
    currentAddressListEntry = (ADDRESSLIST*) malloc(sizeof (ADDRESSLIST));
    if (currentAddressListEntry == 0)
        return; //malloc panik

    //for (i = 0; i < ONEWIRE_ROM_BIT_LENGTH; i++)
    currentAddressListEntry->address = addressFound;
    currentAddressListEntry->connected = 1;
    currentAddressListEntry->next = 0;

    if (lastAddressListEntry != 0)
        lastAddressListEntry->next = currentAddressListEntry;
    else
        *list = currentAddressListEntry;
}

int OneWireROMSeachSpecific(int numberOfPins, TIBOARDPIN tiBoardPins[], ADDRESSLIST **addressList, uint64_t *lastAddress, int *lastConflictPosition) {
    int currentBitIndex = 0;
    uint64_t address = 0LL;
    int currentLastConflictPosition = -1;
    int receiveTwoBits;
    int currentBit;
    int somethingFound = 1;
    //int i;

    if (!OneWireSendReset(numberOfPins, tiBoardPins))
        return 0;
    OneWireSendByte(numberOfPins, tiBoardPins, 0xF0);

    for (currentBitIndex = 0; currentBitIndex < 64; currentBitIndex++) {
        receiveTwoBits = OneWireReceiveBit(numberOfPins, tiBoardPins) << 0;
        receiveTwoBits |= OneWireReceiveBit(numberOfPins, tiBoardPins) << 1;

        if (receiveTwoBits == 3) {
            // Keiner da
            somethingFound = 0;
            break;
        }

        if (receiveTwoBits == 1 || receiveTwoBits == 2) {
            //Eindeutige Adresse das jeweilige Bit nehmen
            if (receiveTwoBits == 1)
                currentBit = 1;
            else
                currentBit = 0;
        } else //if (receiveTwoBits == 0)
        {
            // Gibt Unterschiedliche Bits
            if (currentBitIndex == *lastConflictPosition) {
                currentBit = 1;
            } else if (currentBitIndex < *lastConflictPosition && *lastAddress & (1LL << currentBitIndex)) {
                //Konflikt bereits behandelt
                currentBit = 1;
            } else {
                currentBit = 0;
                currentLastConflictPosition = currentBitIndex;
            }
        }

        address &= ~(1LL << currentBitIndex);
        address |= (uint64_t) currentBit << currentBitIndex;

        OneWireSendBit(numberOfPins, tiBoardPins, currentBit);
    }

    *lastConflictPosition = currentLastConflictPosition;
    //for (i = 0; i < ONEWIRE_ROM_BIT_LENGTH; i++)
    *lastAddress = address;

    if (addressList) {
        if (OneWireIsDeviceConnected(numberOfPins, tiBoardPins, address)) {
            AddressListAddDevice(addressList, address);
            somethingFound = 1;
        } else
            somethingFound = 0;
    } else
        somethingFound = 1;

    return somethingFound;
}

void OneWireROMSeach(int numberOfPins, TIBOARDPIN tiBoardPins[], ADDRESSLIST **addressList) {
    int lastConflictPosition = 64;
    uint64_t currentAddress = 0LL;

    AddressListSetAllDisconnected(*addressList);

    while (lastConflictPosition >= 0) {
        OneWireROMSeachSpecific(numberOfPins, tiBoardPins, addressList, &currentAddress, &lastConflictPosition);
    }
}